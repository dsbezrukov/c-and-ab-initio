#include "Atom.h"

Atom::Atom(const double x, const double y, const double z, const int q)
    : x_(x), y_(y), z_(z), q_(q) {}

int Atom::getQ() const noexcept { return q_; }

double Atom::getX() const noexcept { return x_; }

double Atom::getY() const noexcept { return y_; }

double Atom::getZ() const noexcept { return z_; }
