#pragma once
#include "Molecule.h"
#include <string>

class AOIntegrals {
public:
    AOIntegrals(const Molecule& mol);
    AOIntegrals(const std::string& filename);
    void saveToFile(const std::string& filename) const;
};
