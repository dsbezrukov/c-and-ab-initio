#include "BasisLib.h"
#include "Molecule.h"
#include <fstream>
#include <iomanip>
#include <sstream>
#include <iostream>

int BasisLib::getT(const std::string& name) {
    std::string upperName(name);
    for (auto& c : upperName) c = toupper(c);
    if (dict.find(upperName) == dict.end()) return dict["UNDEF"];
    return dict[upperName];
}

void BasisLib::loadFromGamessUS(const std::string& filename) {
    std::ifstream inp(filename);
    if (not inp) throw std::runtime_error("Problem with *.basis file");
    //  TODO: HomeTask1, realization
    inp.close();
}
